package ru.tsc.chertkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.exception.ProjectNotFoundException;
import ru.tsc.chertkova.tm.exception.user.UserNotFoundException;
import ru.tsc.chertkova.tm.marker.UnitCategory;
import ru.tsc.chertkova.tm.model.dto.ProjectDto;
import ru.tsc.chertkova.tm.util.UserUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest
@RunWith(SpringRunner.class)
@Category(UnitCategory.class)
public class ProjectDtoServiceTest {

    @NotNull
    private static String USER_ID;

    @NotNull
    @Autowired
    private ProjectDtoService projectService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Nullable
    private ProjectDto project;

    @Before
    public void setUp() throws Exception {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
        projectService.clear(USER_ID);
        project = projectService.create(USER_ID);
    }

    @After
    public void tearDown() throws Exception {
        projectService.clear(USER_ID);
    }

    @Test
    public void create() {
        projectService.clear(USER_ID);
        Assert.assertThrows(UserNotFoundException.class, () -> projectService.create(null));
        @NotNull final ProjectDto projectDto = projectService.create(USER_ID);
        Assert.assertNotNull(projectDto);
    }

    @Test
    public void existsById() {
        Assert.assertThrows(UserNotFoundException.class, () -> projectService.existsById(null, project.getId()));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.existsById(USER_ID, null));
        boolean exists = projectService.existsById(USER_ID, project.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void findById() {
        Assert.assertThrows(UserNotFoundException.class, () -> projectService.findById(null, project.getId()));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.findById(USER_ID, null));
        @Nullable final ProjectDto projectDto = projectService.findById(USER_ID, project.getId());
        Assert.assertNotNull(projectDto);
    }

    @Test
    public void findAll() {
        Assert.assertThrows(UserNotFoundException.class, () -> projectService.findAll(null));
        @Nullable final List<ProjectDto> projects = projectService.findAll(USER_ID).stream().collect(Collectors.toList());
        Assert.assertNotNull(projects);
    }

    @Test
    public void count() {
        Assert.assertThrows(UserNotFoundException.class, () -> projectService.count(null));
        long count = projectService.count(USER_ID);
        Assert.assertEquals(1L, count);
    }

    @Test
    public void save() {
        Assert.assertThrows(UserNotFoundException.class, () -> projectService.save(null, project));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.save(USER_ID, null));
        @Nullable ProjectDto projectDto = projectService.findById(USER_ID, project.getId());
        Assert.assertNotNull(projectDto);
        @NotNull final Status newStatus = Status.COMPLETED;
        Assert.assertNotEquals(newStatus, projectDto.getStatus());
        projectDto.setStatus(newStatus);
        projectService.save(USER_ID, projectDto);
        projectDto = projectService.findById(USER_ID, project.getId());
        Assert.assertNotNull(projectDto);
        Assert.assertEquals(projectDto.getStatus(), newStatus);
    }

    @Test
    public void deleteById() {
        Assert.assertThrows(UserNotFoundException.class, () -> projectService.deleteById(null, project.getId()));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.deleteById(USER_ID, null));
        @Nullable ProjectDto projectDto = projectService.findById(USER_ID, project.getId());
        Assert.assertNotNull(projectDto);
        projectService.deleteById(USER_ID, projectDto.getId());
        projectDto = projectService.findById(USER_ID, projectDto.getId());
        Assert.assertNull(projectDto);
    }

    @Test
    public void delete() {
        Assert.assertThrows(UserNotFoundException.class, () -> projectService.delete(null, project));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.delete(USER_ID, null));
        @Nullable ProjectDto projectDto = projectService.findById(USER_ID, project.getId());
        Assert.assertNotNull(projectDto);
        projectService.delete(USER_ID, project);
        projectDto = projectService.findById(USER_ID, projectDto.getId());
        Assert.assertNull(projectDto);
    }

    @Test
    public void deleteAll() {
        List<ProjectDto> projects = new ArrayList<>();
        projects.add(project);
        Assert.assertThrows(UserNotFoundException.class, () -> projectService.deleteAll(null, projects));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.deleteAll(USER_ID, null));
        @Nullable ProjectDto projectDto = projectService.findById(USER_ID, project.getId());
        Assert.assertNotNull(projectDto);
        projectService.deleteAll(USER_ID, projects);
        projectDto = projectService.findById(USER_ID, projectDto.getId());
        Assert.assertNull(projectDto);
    }

    @Test
    public void clear() {
        Assert.assertThrows(UserNotFoundException.class, () -> projectService.clear(null));
        long count = projectService.count(USER_ID);
        Assert.assertEquals(1L, count);
        projectService.clear(USER_ID);
        count = projectService.count(USER_ID);
        Assert.assertEquals(0L, count);
    }

}
