package ru.tsc.chertkova.tm.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class UserDto extends AbstractModelDto {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(name = "email")
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Column(name = "locked")
    private boolean locked = false;

    @NotNull
    @Column(name = "login")
    private String login;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @NotNull
    @Column(name = "password")
    private String passwordHash;

    @NotNull
    @JsonIgnore
    @XmlTransient
    @Column(nullable = false)
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<RoleDto> roles = new ArrayList<>();

    @NotNull
    @Column(name = "created_dt")
    @DateTimeFormat(pattern = FORMAT)
    private Date created = new Date();

    public UserDto(@NotNull final String login, @NotNull final String passwordHash,
                   @NotNull final String email, @Nullable final String firstName,
                   @Nullable final String middleName, @Nullable final String lastName) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
    }

    public UserDto(@NotNull final String login, @NotNull final String passwordHash,
                   @NotNull final String email) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }

    @Override
    public String toString() {
        return "Id " + getId() +
                " email='" + email +
                ", firstName='" + firstName +
                ", lastName='" + lastName +
                ", middleName='" + middleName +
                ", role=" + roles;
    }

}
