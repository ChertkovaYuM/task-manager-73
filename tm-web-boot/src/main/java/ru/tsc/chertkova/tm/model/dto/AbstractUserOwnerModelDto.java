package ru.tsc.chertkova.tm.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public abstract class AbstractUserOwnerModelDto extends AbstractModelDto {

    @Nullable
    @Column(name = "user_id")
    private String userId;

}
