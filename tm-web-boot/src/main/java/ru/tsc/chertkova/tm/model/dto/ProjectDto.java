package ru.tsc.chertkova.tm.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.util.DateUtil;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tm_project")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class ProjectDto extends AbstractUserOwnerModelDto {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(name = "name")
    private String name = "";

    @NotNull
    @Column(name = "description")
    private String description = "";

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(name = "created_dt")
    @DateTimeFormat(pattern = FORMAT)
    private Date created = new Date();

    @Nullable
    @Column(name = "started_dt")
    @DateTimeFormat(pattern = FORMAT)
    private Date dateBegin;

    @Nullable
    @Column(name = "completed_dt")
    @DateTimeFormat(pattern = FORMAT)
    private Date dateEnd;

    public ProjectDto(@NotNull final String name,
                      @NotNull final Status status,
                      @Nullable final Date dateBegin) {
        this.name = name;
        this.status = status;
        this.dateBegin = dateBegin;
    }

    public ProjectDto(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    public ProjectDto(@NotNull final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return getId() + " - " + name + " : " + description + ", " + status + ", " + DateUtil.toString(dateBegin);
    }

}
