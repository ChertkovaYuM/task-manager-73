package ru.tsc.chertkova.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.model.dto.ProjectDto;
import ru.tsc.chertkova.tm.model.dto.TaskDto;
import ru.tsc.chertkova.tm.service.ProjectDtoService;
import ru.tsc.chertkova.tm.service.TaskDtoService;
import ru.tsc.chertkova.tm.util.UserUtil;

import java.util.List;

@Controller
public class TaskController {

    @NotNull
    @Autowired
    private ProjectDtoService projectService;

    @NotNull
    @Autowired
    private TaskDtoService taskService;

    @Nullable
    public List<ProjectDto> getProjects() {
        return projectService.findAll(UserUtil.getUserId());
    }

    @Nullable
    public List<TaskDto> getTasks(@NotNull final String userId) {
        return taskService.findAll(UserUtil.getUserId());
    }

    @NotNull
    public Status[] getStatuses() {
        return Status.values();
    }

    @NotNull
    @GetMapping("/task/create")
    public String create() {
        taskService.create(UserUtil.getUserId());
        return "redirect:/tasks";
    }

    @GetMapping("/tasks")
    public ModelAndView list() {
        return new ModelAndView("tasks", "tasks", getTasks(UserUtil.getUserId()));
    }

    @NotNull
    @PostMapping("/task/edit/{id}")
    public String edit(
            @NotNull @ModelAttribute("task") final TaskDto task,
            @NotNull final BindingResult result
    ) {
        taskService.save(UserUtil.getUserId(), task);
        return "redirect:/tasks";
    }


    @NotNull
    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@NotNull @PathVariable("id") final String taskId) {
        @NotNull final TaskDto task = taskService.findById(UserUtil.getUserId(), taskId);
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", getProjects());
        return modelAndView;
    }

    @NotNull
    @GetMapping("/task/delete/{id}")
    public String delete(@NotNull @PathVariable("id") final String id) {
        taskService.deleteById(UserUtil.getUserId(), id);
        return "redirect:/tasks";
    }

}
