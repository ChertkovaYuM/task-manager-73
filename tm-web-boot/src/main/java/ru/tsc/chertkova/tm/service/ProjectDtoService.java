package ru.tsc.chertkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.chertkova.tm.api.repository.IProjectDtoRepository;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.exception.ProjectNotFoundException;
import ru.tsc.chertkova.tm.exception.user.UserNotFoundException;
import ru.tsc.chertkova.tm.model.dto.ProjectDto;

import java.util.Date;
import java.util.List;

@Service
public class ProjectDtoService {

    @NotNull
    @Autowired
    private IProjectDtoRepository repository;

    @NotNull
    @Modifying
    @Transactional
    public ProjectDto create(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final ProjectDto project = new ProjectDto(
                "New project: " + System.currentTimeMillis(),
                Status.IN_PROGRESS,
                new Date());
        project.setUserId(userId);
        save(userId, project);
        return project;
    }

    @NotNull
    @Modifying
    @Transactional
    public ProjectDto create(@Nullable final String userId,
                             @NotNull final String name) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final ProjectDto project = new ProjectDto(
                name,
                "Описание");
        project.setUserId(userId);
        save(userId, project);
        return project;
    }

    @NotNull
    @Modifying
    @Transactional
    public ProjectDto save(@Nullable final String userId,
                           @Nullable final ProjectDto project) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (project == null) throw new ProjectNotFoundException();
        project.setUserId(userId);
        return repository.save(project);
    }

    @Nullable
    public List<ProjectDto> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return repository.findAllByUserId(userId);
    }

    @Nullable
    public ProjectDto findById(@Nullable final String userId,
                               @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (id == null || id.isEmpty()) throw new ProjectNotFoundException();
        return repository.findByUserIdAndId(userId, id);
    }

    @Modifying
    @Transactional
    public void deleteById(@Nullable final String userId,
                           @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (id == null || id.isEmpty()) throw new ProjectNotFoundException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Modifying
    @Transactional
    public void delete(@Nullable final String userId,
                       @Nullable final ProjectDto project) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (project == null) throw new ProjectNotFoundException();
        deleteById(userId, project.getId());
    }

    @Modifying
    @Transactional
    public void deleteAll(@Nullable final String userId,
                          @Nullable final List<ProjectDto> projects) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (projects == null) throw new ProjectNotFoundException();
        projects
                .stream()
                .forEach(project -> delete(userId, project));
    }

    public boolean existsById(@Nullable final String userId,
                              @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (id == null || id.isEmpty()) throw new ProjectNotFoundException();
        return repository.existsByUserIdAndId(userId, id);
    }

    @Modifying
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        repository.deleteByUserId(userId);
    }

    public long count(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return repository.countByUserId(userId);
    }

}
