package ru.tsc.chertkova.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.model.dto.ProjectDTO;
import ru.tsc.chertkova.tm.model.dto.TaskDTO;
import ru.tsc.chertkova.tm.service.ProjectService;
import ru.tsc.chertkova.tm.service.TaskService;
import ru.tsc.chertkova.tm.util.UserUtil;

import java.util.Collection;

@Controller
public class TaskController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private ProjectService projectService;

    @NotNull
    private Collection<TaskDTO> getTasks() {
        return taskService.findAll(UserUtil.getUserId());
    }

    @Nullable
    private Collection<ProjectDTO> getProjects() {
        return projectService.findAll(UserUtil.getUserId());
    }

    @GetMapping("/task/create")
    public String create() {
        taskService.create(UserUtil.getUserId());
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        taskService.deleteById(UserUtil.getUserId(),id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    public String edit(@ModelAttribute("task") TaskDTO task, BindingResult result) {
        taskService.save(UserUtil.getUserId(),task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        @NotNull final TaskDTO task = taskService.findById(UserUtil.getUserId(), id);
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("statuses", getStatuses());
        modelAndView.addObject("projects", getProjects());
        return modelAndView;
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

    @NotNull
    @GetMapping("/tasks")
    public ModelAndView list() {
        return new ModelAndView("task-list", "tasks", getTasks());
    }

}
