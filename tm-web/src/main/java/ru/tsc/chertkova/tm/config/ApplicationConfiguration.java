package ru.tsc.chertkova.tm.config;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import ru.tsc.chertkova.tm.model.dto.AbstractModelDTO;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.tsc.chertkova.tm")
@PropertySource("classpath:application.properties")
@EnableJpaRepositories("ru.tsc.chertkova.tm.api.repository")
public class ApplicationConfiguration extends WsConfigurerAdapter {

    @NotNull
    private static final String HAZELCAST_USE_LITE_MEMBER = "hibernate.cache.hazelcast.use_lite_member";

    @Bean
    @NotNull
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @NotNull
    public DataSource dataSource(
            @NotNull @Value("#{environment['database.driver']}") final String databaseDriver,
            @NotNull @Value("#{environment['database.url']}") final String databaseConnectionString,
            @NotNull @Value("#{environment['database.username']}") final String databaseUser,
            @NotNull @Value("#{environment['database.password']}") final String databasePassword
    ) {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(databaseDriver);
        dataSource.setUrl(databaseConnectionString);
        dataSource.setUsername(databaseUser);
        dataSource.setPassword(databasePassword);
        return dataSource;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NotNull final DataSource dataSource,
            @NotNull @Value("#{environment['database.dialect']}") final String dialect,
            @NotNull @Value("#{environment['database.hbm2ddl_auto']}") final String hbm2Ddl,
            @NotNull @Value("#{environment['database.show_sql']}") final String showSql,
            @NotNull @Value("#{environment['database.format_sql']}") final String formatSql,
            @NotNull @Value("#{environment['database.cache.use_second_level_cache']}") final String useSecondLvlCache,
            @NotNull @Value("#{environment['database.cache.provider_configuration_file_resource_path']}") final String cacheProviderConfigFile,
            @NotNull @Value("#{environment['database.cache.region.factory_class']}") final String cacheRegionFactoryClass,
            @NotNull @Value("#{environment['database.cache.use_query_cache']}") final String cacheUseQueryCache,
            @NotNull @Value("#{environment['database.cache.use_minimal_puts']}") final String cacheUseMinPuts,
            @NotNull @Value("#{environment['database.cache.region_prefix']}") final String cacheRegionPrefix,
            @NotNull @Value("#{environment['database.cache.hazelcast.use_lite_member']}") final String cacheHazelcastUseLiteMember
    ) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan(
                AbstractModelDTO.class.getPackage().getName()
        );
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DIALECT, dialect);
        properties.put(Environment.HBM2DDL_AUTO, hbm2Ddl);
        properties.put(Environment.SHOW_SQL, showSql);
        properties.put(Environment.FORMAT_SQL, formatSql);
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, useSecondLvlCache);
        properties.put(Environment.CACHE_PROVIDER_CONFIG, cacheProviderConfigFile);
        properties.put(Environment.CACHE_REGION_FACTORY, cacheRegionFactoryClass);
        properties.put(Environment.USE_QUERY_CACHE, cacheUseQueryCache);
        properties.put(Environment.USE_MINIMAL_PUTS, cacheUseMinPuts);
        properties.put(Environment.CACHE_REGION_PREFIX, cacheRegionPrefix);
        properties.put(HAZELCAST_USE_LITE_MEMBER, cacheHazelcastUseLiteMember);
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

}
