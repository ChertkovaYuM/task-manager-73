<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp">
 <jsp:param name="title" value="Tasks"/>
</jsp:include>
<h1>Tasks</h1>
<table>
    <tr>
        <th>#</th>
        <th>Id</th>
        <th>Name</th>
        <th>Status</th>
        <th>Description</th>
        <th>Created</th>
        <th>Started</th>
        <th>Completed</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
    <c:forEach var="task" items="${tasks}" varStatus="status">
        <tr>
            <td>${status.index + 1}</td>
            <td>${task.id}</td>
            <td>${task.name}</td>
            <td>${task.status.displayName}</td>
            <td>${task.description}
            <td>
                <fmt:formatDate value="${task.created}" pattern="yyyy-MM-dd kk:mm"/>
            </td>
            <td>
                <fmt:formatDate value="${task.dateBegin}" pattern="yyyy-MM-dd kk:mm"/>
            </td>
            <td>
                <fmt:formatDate value="${task.dateEnd}" pattern="yyyy-MM-dd kk:mm"/>
            </td>
            <td>
                <form:form class="mini" method="get" action="/task/edit/${task.id}">
                    <button type="submit">Edit</button>
                </form:form>
            </td>
            <td>
                <form:form class="mini" method="get" action="/task/delete/${task.id}">
                    <button type="submit">Delete</button>
                </form:form>
            </td>
        </tr>
    </c:forEach>
</table>
<form:form method="get" action="/task/create">
    <button type="submit">Create</button>
</form:form>
<jsp:include page="../include/_footer.jsp" />